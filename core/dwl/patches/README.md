autostart:

Allow dwl to execute commands from autostart array in your config.h 
file. And when you exit dwl all processes from autostart array will be 
killed.

shiftview:

Adds shiftview function that allows to cycle through tags.

vanitygaps:

Adds (inner) gaps between client windows and (outer) gaps between 
windows and the screen edge in a flexible manner.

squibidmoveKeyboard:

Add abillity to resize the floating window
