Homepage:
                https://sr.ht/~raphi/somebar/

                tar xvJf somebar-1.0.3.tar.gz 
                or 
                git clone https://git.sr.ht/~raphi/somebar

Configuration:
                Copy src/config.def.hpp to src/config.hpp, and adjust 
                if needed.

Patching: 
(optional)
                patch -p1 < contrib/hide-vacant-tags.patch
                patch -p1 < markup-in-status-messages.patch
Building:
                cp src/config.def.hpp src/config.hpp
                meson setup --prefix=/usr \
                --libdir=/usr/lib64 \
                --sysconfir=/etc \
                --mandir=/usr/man 
                
                meson -C build
                sudo meson install  -C build 

Usage:
                You must start somebar using dwl's -s flag, e.g. 
                dwl -s somebar.

Somebar can be controlled by writing to $XDG_RUNTIME_DIR/somebar-0 or 
the path defined by -s argument. The following commands are supported:

- status TEXT: Updates the status bar
- hide MONITOR Hides somebar on the specified monitor
- show MONITOR Shows somebar on the specified monitor
- toggle MONITOR Toggles somebar on the specified monitor



Examples: Script to update your statusbar


dwmtime() {
        clock="$(date +"%A, %B %d - %H:%M")"
        icon=""
        printf "%s %s\n" "$icon" "$clock"
}

while true; do
        somebar -c status "$(dwltime)"
        sleep 1s
done
