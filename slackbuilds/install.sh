#!/bin/bash
#
src=(
libdrm
wayland
wayland-protocols
tllist
wlr-randr
uthash
scdoc
seatd
wlroots
slurp
grim
wbg
lbonn-rofi-wayland
ttf-font-awesome
#ttf-font-awesome-4
ttf-font-fork-awesome
fcft
#dwl
#dwlb-git
brave-browser
)

for i in ${src[@]}; do
	cd $i || exit 1
	sh $i.SlackBuild || exit 1
	upgradepkg --install-new --reinstall /tmp/$i*_SBo.t?z || exit 1
	cd ..
done


